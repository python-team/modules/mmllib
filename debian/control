Source: mmllib
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Dominik George <nik@naturalnet.de>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-setuptools,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://edugit.org/Teckids/hacknfun/libs/mmllib
Testsuite: autopkgtest-pkg-pybuild
Vcs-Git: https://salsa.debian.org/python-team/packages/mmllib.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/mmllib

Package: python3-mmllib
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: library for handling Music Macro Language (Python 3)
 MMLlib is a pure Python implementation of functionality related to the
 Music Macro Language as implemented by Microsoft® GW-BASIC® and
 compatibles, which is its most common form, also implemented by the PC
 speaker driver in Linux and BSD, with a number of extensions and
 changes.
 .
 This package contains the Python 3 build.

Package: mmllib-tools
Architecture: all
Section: sound
Depends:
 python3-mmllib,
 ${misc:Depends},
 ${python3:Depends},
Description: library for handling Music Macro Language (tools)
 MMLlib is a pure Python implementation of functionality related to the
 Music Macro Language as implemented by Microsoft® GW-BASIC® and
 compatibles, which is its most common form, also implemented by the PC
 speaker driver in Linux and BSD, with a number of extensions and
 changes.
 .
 This package contains command-line tools for handling MML. These
 currently are:
 .
  mml2musicxml - convert MML to MusicXML (cf. musescore)
  mmllint - check various aspects of MML files for correctness
